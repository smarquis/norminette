Bug-Database: https://github.com/42School/norminette/issues
Bug-Submit: https://github.com/42School/norminette/issues/new
Repository-Browse: https://github.com/42School/norminette
Repository: https://github.com/42School/norminette.git
